import * as wasm from "wasm-interpreter";

const input = document.getElementById('expression');
const form = document.getElementById('expression-form');

form.addEventListener('submit', function (event) {
    if (event.preventDefault) event.preventDefault();
    const expression = input.value;
    const res = wasm.evaluate(expression)
    alert(expression + ' = ' + res);
    return false;
});
