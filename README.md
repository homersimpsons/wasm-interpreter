# Wasm Interpreter

A Small Wasm Interpreter written in rust.

Can parse and compute expression like:
- `12+23` => 34
- `3/2` => 1 (only integers)

# Set-up locally

1. `git clone` the project
2. Ensure you have the [required dependencies](https://rustwasm.github.io/docs/book/game-of-life/setup.html)
3. run `wasm-pack build` in `.`
4. run `npm install` in `www`
5. run `npm run start` in `www` and visit http://localhost:8080/

# About

This project uses the following references:
- [Rust Wasm Book](https://rustwasm.github.io/docs/book/introduction.html)
- [nom crate](https://docs.rs/nom/5.1.1/nom/index.html)
