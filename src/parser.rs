use nom::{
    branch::alt,
    character::complete::{char, digit1},
    combinator::map,
};

use crate::token::{Op, Token};

pub fn parse(input: &[u8]) -> Token {
    parse_next_token(input).unwrap().1
}

#[derive(Debug, PartialEq)]
enum Operator {
    /// +
    ADD,
    /// -
    SUB,
    /// *
    MUL,
    /// /
    DIV,
}

fn parse_next_token(input: &[u8]) -> nom::IResult<&[u8], Token> {
    alt((parse_operation, parse_int))(input)
}

fn from_digit(input: &[u8]) -> Token {
    Token::Integer(i64::from_str_radix(std::str::from_utf8(input).unwrap(), 10).unwrap())
}

fn parse_int(input: &[u8]) -> nom::IResult<&[u8], Token> {
    map(digit1, from_digit)(input)
}

fn parse_operation(input: &[u8]) -> nom::IResult<&[u8], Token> {
    let (input, lhs) = parse_int(input)?;
    let (input, op) = parse_operator(input)?;
    let (input, rhs) = parse_int(input)?;
    Ok((input, fold_op(lhs, op, rhs)))
}

fn fold_op(lhs: Token, op: Operator, rhs: Token) -> Token {
    match op {
        Operator::ADD => Token::Op(Op::Add(Box::new(lhs), Box::new(rhs))),
        Operator::SUB => Token::Op(Op::Sub(Box::new(lhs), Box::new(rhs))),
        Operator::MUL => Token::Op(Op::Mul(Box::new(lhs), Box::new(rhs))),
        Operator::DIV => Token::Op(Op::Div(Box::new(lhs), Box::new(rhs))),
    }
}

fn parse_operator(input: &[u8]) -> nom::IResult<&[u8], Operator> {
    alt((
        map(char('+'), |_| Operator::ADD),
        map(char('-'), |_| Operator::SUB),
        map(char('*'), |_| Operator::MUL),
        map(char('/'), |_| Operator::DIV),
    ))(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        assert_eq!(
            parse(b"1-2"),
            Token::Op(Op::Sub(
                Box::new(Token::Integer(1)),
                Box::new(Token::Integer(2))
            )),
        );
    }
    #[test]
    fn test_operation() {
        assert_eq!(
            parse_operation(b"1+2"),
            Ok((
                &b""[..],
                Token::Op(Op::Add(
                    Box::new(Token::Integer(1)),
                    Box::new(Token::Integer(2))
                ))
            ))
        );
    }
    #[test]
    fn test_operator() {
        assert_eq!(parse_operator(b"+"), Ok((&b""[..], Operator::ADD)));
        assert_eq!(parse_operator(b"-"), Ok((&b""[..], Operator::SUB)));
        assert_eq!(parse_operator(b"*"), Ok((&b""[..], Operator::MUL)));
        assert_eq!(parse_operator(b"/"), Ok((&b""[..], Operator::DIV)));
    }
}
