#[derive(Debug, PartialEq)]
pub enum Token {
    Integer(i64),
    Op(Op),
}

#[derive(Debug, PartialEq)]
pub enum Op {
    Add(Box<Token>, Box<Token>),
    Sub(Box<Token>, Box<Token>),
    Mul(Box<Token>, Box<Token>),
    Div(Box<Token>, Box<Token>),
}
