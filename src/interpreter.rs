use crate::token::{Op, Token};

pub fn interpret(token: Token) -> i64 {
    match token {
        Token::Integer(v) => v,
        Token::Op(op) => compute(op),
    }
}

fn compute(op: Op) -> i64 {
    match op {
        Op::Add(lhs, rhs) => interpret(*lhs) + interpret(*rhs),
        Op::Sub(lhs, rhs) => interpret(*lhs) - interpret(*rhs),
        Op::Mul(lhs, rhs) => interpret(*lhs) * interpret(*rhs),
        Op::Div(lhs, rhs) => interpret(*lhs) / interpret(*rhs),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_interpret() {
        assert_eq!(
            interpret(Token::Op(Op::Add(
                Box::new(Token::Integer(1)),
                Box::new(Token::Integer(2))
            ))),
            3
        );
        assert_eq!(
            interpret(Token::Op(Op::Sub(
                Box::new(Token::Integer(1)),
                Box::new(Token::Integer(2))
            ))),
            -1
        );
        assert_eq!(
            interpret(Token::Op(Op::Mul(
                Box::new(Token::Integer(4)),
                Box::new(Token::Integer(12))
            ))),
            48
        );
        assert_eq!(
            interpret(Token::Op(Op::Div(
                Box::new(Token::Integer(10)),
                Box::new(Token::Integer(2))
            ))),
            5
        );
    }
}
