use wasm_bindgen::prelude::*;

mod interpreter;
mod parser;
mod token;
mod utils;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn evaluate(expression: &str) -> i64 {
    let op = parser::parse(expression.as_bytes());
    interpreter::interpret(op)
}
